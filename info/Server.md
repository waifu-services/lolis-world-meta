# Server info / specs

## Hardware

Lolis.World runs on the host [Yui](https://yui.waifu.work/), which is a Dedicated from [Hetzner](http://hetzner.cloud/) with the model name "SB27" and the following specs:

- 8 CPU core (Intel Xeon E3-1246V3)
- 32GB RAM
- 4 TB HDD Entreprise
- 20TB Transfer

It is located in Nuremberg, Germany. (Hetzner's "NBG1-DC1")

## Software

Yui runs Debian 9.5 and currently hosts every services of Waifu Services, and a discord bot called [Sagiri](https://sagiri.party) exclusively.

Pleroma runs with an unprivileged user (no sudo access) and cannot see processes initiated by other users in applications such as `htop`. Pleroma is configured to be reachable over both IPv4 and IPv6, while pf prevents externals from accessing Pleroma without passing through nginx.

Monitoring of the server is handled by [nixstats](https://nixstats.com/)